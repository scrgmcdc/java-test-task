package products;

public class Laptop extends AbstractDisplay {

	// in hours
	public float batteryLifeTime;

	public float getBatteryLifeTime() {
		return batteryLifeTime;
	}

	public void setBatteryLifeTime(float batteryLifeTime) {
		this.batteryLifeTime = batteryLifeTime;
	}
}
