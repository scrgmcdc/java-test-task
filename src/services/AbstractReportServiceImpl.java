package services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import products.AbstractDisplay;

abstract class AbstractReportServiceImpl implements ReportService {

	protected String delimFirst;
	protected String delimId;
	protected String delimName;
	protected String delimEnd;

	@Override
	public String generateUniqueProductReport(List<AbstractDisplay> products) {
		Set<AbstractDisplay> uniqueProducts = new HashSet<AbstractDisplay>();
		StringBuilder report = new StringBuilder();
		for (AbstractDisplay product : products) {
			if (!contains(product, uniqueProducts)) {
				uniqueProducts.add(product);
				report.append(delimFirst);
				report.append(product.getId());
				report.append(delimId);
				report.append(product.getName());
				report.append(delimName);
				report.append(product.getPrice());
				report.append(delimEnd);
			}
		}
		return report.toString();
	}

	protected boolean contains(AbstractDisplay product, Set<AbstractDisplay> uniqueProducts) {
		for (AbstractDisplay uniqueProduct : uniqueProducts) {
			if (uniqueProduct.getClass() != product.getClass()) return false;
			if (uniqueProduct.getId() != product.getId()) return false;
			return true;
		}
		return false;
	}

}
