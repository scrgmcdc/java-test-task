package services;

/**
 * CSV report service
 */
public class CSVReportService extends AbstractReportServiceImpl {

	public CSVReportService() {
		super();
		delimFirst = "";
		delimId = ";";
		delimName = "";
		delimEnd = "\n";
	}
}
