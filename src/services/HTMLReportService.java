package services;

/**
 * Html report service.
 */
public class HTMLReportService extends AbstractReportServiceImpl {

	public HTMLReportService() {
		super();
		delimFirst = "<div>";
		delimId = "</div><div>";
		delimName = "</div><div>";
		delimEnd = "</div>\n";
	}

}
