package services;

import java.io.IOException;

/**
 * Main service.
 */
public class MainService {

	// Injected using DI (e.g. spring)
	ProductService productService;
	// Injected using DI (e.g. spring)
	HTMLReportService htmlReportService;
	// Injected using DI (e.g. spring)
	ReportService csvReportService;

	public void prinntReport(String name) {
		try {
			if (name == "CSV") {
				String report = csvReportService
						.generateUniqueProductReport(productService
								.getAllProducts());
				print(report);
			} else if (name == "HTML") {
				String report = htmlReportService
						.generateUniqueProductReport(productService
								.getAllProducts());
				print(report);
			}
		} catch (Throwable throwable) {
			// print error to console
			System.out.println(throwable);
			// Exit method without error
			System.exit(0);
		}
	}

	/**
	 * Prints given string to some printer.
	 *
	 * @param report
	 * @throws IOException
	 */
	void print(String report) throws IOException {
		// PRINT REPORT
		// STUB
	}
}
