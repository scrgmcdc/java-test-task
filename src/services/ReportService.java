package services;

import java.util.List;

import products.AbstractDisplay;

public interface ReportService {

	public abstract String generateUniqueProductReport(
			List<AbstractDisplay> products);

}